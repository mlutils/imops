import cv2
import numpy as np
from scipy import ndimage
import sympy
from math import atan2, cos, sin, sqrt, pi
from skimage.morphology import disk, skeletonize    
from scipy.spatial import distance
from skimage.color import rgb2lab
import matplotlib.pyplot as plt
from skimage.measure import label, regionprops, regionprops_table
import mlutils.imops.image_operations as iops
import math


def drawAxis(img, p_, q_, color, scale):
      p = list(p_)
      q = list(q_)
     
      ## [visualization1]
      angle = atan2(p[1] - q[1], p[0] - q[0]) # angle in radians
      hypotenuse = sqrt((p[1] - q[1]) * (p[1] - q[1]) + (p[0] - q[0]) * (p[0] - q[0]))
     
      # Here we lengthen the arrow by a factor of scale
      q[0] = p[0] - scale * hypotenuse * cos(angle)
      q[1] = p[1] - scale * hypotenuse * sin(angle)
      cv2.line(img, (int(p[0]), int(p[1])), (int(q[0]), int(q[1])), color, 3, cv2.LINE_AA)
     
      # create the arrow hooks
      p[0] = q[0] + 9 * cos(angle + pi / 4)
      p[1] = q[1] + 9 * sin(angle + pi / 4)
      cv2.line(img, (int(p[0]), int(p[1])), (int(q[0]), int(q[1])), color, 3, cv2.LINE_AA)
     
      p[0] = q[0] + 9 * cos(angle - pi / 4)
      p[1] = q[1] + 9 * sin(angle - pi / 4)
      cv2.line(img, (int(p[0]), int(p[1])), (int(q[0]), int(q[1])), color, 3, cv2.LINE_AA)
      ## [visualization1]
      
      
def rotate_image(image, angle):
      image_center = tuple(np.array(image.shape[1::-1]) / 2)
      rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
      result = cv2.warpAffine(image, rot_mat, image.shape[1::-1], flags=cv2.INTER_LINEAR)
      return result
  
    
def measure_ellipse_area_and_diameter(img, mask, cm_per_pixel, draw_lines=1):
    regions = regionprops(mask)
    props = regions[0]
    y0, x0 = props.centroid
    orientation = props.orientation
    rmin = props.axis_minor_length 
    rmax = props.axis_major_length 
    xx1 = x0 + math.cos(orientation) * 0.5 * rmin
    yy1 = y0 - math.sin(orientation) * 0.5 * rmin
    xx2 = x0 - math.sin(orientation) * 0.5 * rmax
    yy2 = y0 - math.cos(orientation) * 0.5 * rmax

    # Draw lines (radius and diameter)
    d0 = np.int0((x0, y0))
    d1 = np.int0((xx1, yy1))
    d2 = np.int0((xx2, yy2))
    if draw_lines == 1:
        cv2.line(img, d0, d1, (0, 0, 255), 5)
        cv2.line(img, d0, d2, (0, 0, 255), 5)

    rmin_cm = props.axis_minor_length * cm_per_pixel
    rmax_cm = props.axis_major_length * cm_per_pixel

    diameter = round(rmax_cm*2, 4)
    area = round(math.pi * rmax_cm * rmin_cm, 4)
    results = {"rmin": rmin, "rmax": rmax, "rmin_cm": rmin_cm, "rmax_cm": rmax_cm, "diameter": diameter, "area": area}
    return results

  
"""
three_channel_mean
- Gets 3-channel mean from an image

Input:
img = RGB or BGR image in (cv2 mat)
m = mask (np.uint8)

Output:
[A, B, C], [A_ave, B_ave, C_ave] 

Projects:
FSDT
"""
def three_channel_mean(img, m):
    A,B,C = cv2.split(img)
    # A_ave, B_ave, C_ave = cv2.mean(img, mask=m)[:3]
    # A_ave = round(A_ave, 2)
    # B_ave = round(B_ave, 2)
    # C_ave = round(C_ave, 2)
    A = m*A
    B = m*B
    C = m*C
    A_ave = np.average(A[np.where(A!=0)])
    B_ave = np.average(B[np.where(B!=0)])
    C_ave = np.average(C[np.where(C!=0)])

    return [A, B, C], [A_ave, B_ave, C_ave] 
  
    


  
"""
get_color_info
- Gets RGB and HSV color info from an image

Input:
img = RGB or BGR image in (cv2 mat)
m = mask (np.uint8)
input_format = "BGR" or "RGB"
show_output = 0 or 1

Output:
r_ave, g_ave, b_ave, h_ave, s_ave, v_ave

Projects:
FSDT
"""
import copy
def measure_colors(img, b, m, input_format="BGR", show_output=0):
    img_cv2 = copy.deepcopy(img)
    h, w, _ = img_cv2.shape

    # m = iops.polygon2mask(m, h, w)
    m = np.uint8(m*255)
    m = iops.resize_with_padding(m, [h, w])
    mx = cv2.bitwise_and(img_cv2, img_cv2, mask=m)

    b = np.int0(b)
    img_cv2 = img_cv2[b[1]:b[3], b[0]:b[2]]
    m = m[b[1]:b[3], b[0]:b[2]]
    
    if input_format == "BGR":
        rgb = cv2.cvtColor(img_cv2, cv2.COLOR_BGR2RGB)
    else:
        rgb = img_cv2
    hsv = cv2.cvtColor(img_cv2, cv2.COLOR_BGR2HSV)

    lab = rgb2lab(rgb)
    lab_scaled = (lab + [0, 128, 128]) / [100, 255, 255]
    
    cmy = iops.rgb2cmy(rgb)

    
    [rgb_r, rgb_g, rgb_b], rgb_ave = three_channel_mean(rgb, m)
    [hsv_h, hsv_s, hsv_v], hsv_ave = three_channel_mean(hsv, m)
    [lab_l, lab_a, lab_b], lab_ave = three_channel_mean(lab_scaled, m)
    [cmy_c, cmy_m, cmy_y], cmy_ave = three_channel_mean(cmy, m)




    colors = {"rgb": [float("{:.4f}".format(c/255)) for c in rgb_ave],
              "hsv": [float("{:.4f}".format(c/255)) for c in hsv_ave],
              "lab": [float("{:.4f}".format(c/255)) for c in lab_ave],
              "cmy": [float("{:.4f}".format(c)) for c in cmy_ave]}


    if show_output == 1:
        plt.figure(dpi=300)
        plt.subplot(4,3,1)
        plt.title("Masked")
        plt.imshow(mx[...,::-1])
        plt.axis(False)
        plt.subplot(4,3,2)
        plt.title("Mask")
        plt.imshow(m, cmap="gray")
        plt.axis(False)
        
        plt.subplot(4,3,4)
        plt.title("R={:.2f}".format(rgb_ave[0]/255))
        plt.imshow(rgb_r, cmap="gray")
        plt.axis(False)
        plt.subplot(4,3,5)
        plt.title("G={:.2f}".format(rgb_ave[1]/255))
        plt.imshow(rgb_g, cmap="gray")
        plt.axis(False)
        plt.subplot(4,3,6)
        plt.title("B={:.2f}".format(rgb_ave[2]/255))
        plt.imshow(rgb_b, cmap="gray")
        plt.axis(False)
        
        plt.subplot(4,3,7)
        plt.title("H={:.2f}".format(hsv_ave[0]/255))
        plt.imshow(hsv_h, cmap="gray")
        plt.axis(False)
        plt.subplot(4,3,8)
        plt.title("S={:.2f}".format(hsv_ave[1]/255))
        plt.imshow(hsv_s, cmap="gray")
        plt.axis(False)
        plt.subplot(4,3,9)
        plt.title("V={:.2f}".format(hsv_ave[2]/255))
        plt.imshow(hsv_v, cmap="gray")
        plt.axis(False)

        plt.subplot(4,3,10)
        plt.title("L={:.2f}".format(lab_ave[0]/255))
        plt.imshow(lab_l, cmap="gray")
        plt.axis(False)
        plt.subplot(4,3,11)
        plt.title("a={:.2f}".format(lab_ave[1]/255))
        plt.imshow(lab_a, cmap="gray")
        plt.axis(False)
        plt.subplot(4,3,12)
        plt.title("b={:.2f}".format(lab_ave[2]/255))
        plt.imshow(lab_b, cmap="gray")
        plt.axis(False)
        
        plt.subplots_adjust(hspace=0.8)
        plt.show()


    return colors 




def get_max_contour(m):
    cnts, hierarchy = cv2.findContours(np.asarray(m).astype(np.uint8), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)[-2:]
    if len(cnts) > 0:
        max_contour = sorted(cnts, key=cv2.contourArea, reverse= True)[0]
        return max_contour

def get_max_contour_hierarchy(m):
    cnts, hierarchy = cv2.findContours(np.asarray(m).astype(np.uint8), cv2.RETR_CCOMP, cv2.CHAIN_APPROX_NONE)[-2:]
    if len(cnts) > 0:
        max_contour = [sorted(cnts, key=cv2.contourArea, reverse= True)[0]]
        return max_contour, hierarchy

def fit_n_gon(mask, n=4):
    contours, hierarchy = cv2.findContours(mask,
                                           cv2.RETR_EXTERNAL,
                                           cv2.CHAIN_APPROX_SIMPLE)
    hull = cv2.convexHull(contours[0])
    epsilon = 0.1*cv2.arcLength(contours[0],True)
    n_iter, max_iter = 0, 100
    lb, ub = 0., 1.

    while True:
        n_iter += 1
        if n_iter > max_iter:
            return hull, contours

        k = (lb + ub)/2.
        eps = k*cv2.arcLength(hull, True)
        approx = cv2.approxPolyDP(hull, eps, True)

        if len(approx) > n:
            lb = (lb + ub)/2.
        elif len(approx) < n:
            ub = (lb + ub)/2.
        else:
            return approx, contours




def show_ngon_lengths(img, n_polygon, n_edges, edge_color=(125,0,0), text_color=(0,255,0)):
    img_output = img.copy()
    cv2.drawContours(img_output, [n_polygon], 0, edge_color, 1)
    for i, sides in enumerate(n_polygon):
        j = i+1
        if i == n_edges-1:
            j = 0
        try:
            c1 = n_polygon[i][0]
            c2 = n_polygon[j][0] 
            dx = int(distance.euclidean(c1, c2))
            
            m = [int((c1[0]+c2[0])/2), int((c1[1]+c2[1])/2)]
            m_text = [int((c1[0]+c2[0])/2)-20, int((c1[1]+c2[1])/2)]
            cv2.circle(img_output, m, radius=3, color=(255, 0, 0), thickness=-1)
            
            cv2.putText(img_output, str(dx), m_text,  cv2.FONT_HERSHEY_SIMPLEX, 0.5, text_color, 1, cv2.LINE_AA)
        except:
            pass
    
    
    return img_output




def orientation_correction(contours, img, fill_contour=1, reshape=True, debug=0):
    img_output = img.copy()
    
    angle = 0
    for i, pts in enumerate(contours):
        area = cv2.contourArea(pts)
        if area < 3700 or 100000 < area:
          continue
        if fill_contour == 1:
            cv2.drawContours(img_output, contours, i, (255, 255, 255), -1)
        else:
            cv2.drawContours(img_output, contours, i, (255, 255, 255), 2)
        

        # Construct a buffer used by the pca analysis
        sz = len(pts)
        data_pts = np.empty((sz, 2), dtype=np.float64)
        for i in range(data_pts.shape[0]):
          data_pts[i,0] = pts[i,0,0]
          data_pts[i,1] = pts[i,0,1]
       
        # Perform PCA analysis
        mean = np.empty((0))
        mean, eigenvectors, eigenvalues = cv2.PCACompute2(data_pts, mean)
       
        # Store the center of the object
        cntr = (int(mean[0,0]), int(mean[0,1]))
    
        p1 = (cntr[0] + 0.02 * eigenvectors[0,0] * eigenvalues[0,0], cntr[1] + 0.02 * eigenvectors[0,1] * eigenvalues[0,0])
        p2 = (cntr[0] - 0.02 * eigenvectors[1,0] * eigenvalues[1,0], cntr[1] - 0.02 * eigenvectors[1,1] * eigenvalues[1,0])
        angle = atan2(eigenvectors[0,1], eigenvectors[0,0]) # orientation in radians
        

    angle = int(np.rad2deg(angle))
    

    # Rotate
    img_rotate = ndimage.rotate(img.copy(), angle, reshape=reshape)
    img_mask_rotate = ndimage.rotate(img_output.copy(), angle, reshape=reshape)    


    

    # Unpad the image
    gray = cv2.cvtColor(img_mask_rotate, cv2.COLOR_BGR2GRAY)
    contours, _ = cv2.findContours(gray, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
    for contour in contours:
        area = cv2.contourArea(contour)
        if area < 3700 or 100000 < area:
            continue
        (x,y,w,h) = cv2.boundingRect(contour)
    img_rotate_unpadded = img_rotate[y:y+h, x:x+w]
    img_mask_rotate_unpadded = img_mask_rotate[y:y+h, x:x+w]
    
    # Convert to binary
    img_mask_rotate_unpadded = cv2.cvtColor(img_mask_rotate_unpadded, cv2.COLOR_BGR2GRAY)
    _, img_mask_rotate_unpadded = cv2.threshold(img_mask_rotate_unpadded, 50, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
    
    
    # Get centers; needed for coordinate rotation
    org_center = (np.array(img.shape[:2][::-1])-1)/2.
    rot_center = (np.array(img_rotate_unpadded.shape[:2][::-1])-1)/2.


    if debug == 1:
        plt.figure(dpi=150)
        plt.subplot(1,3,1)
        plt.imshow(img)
        plt.subplot(1,3,2)
        plt.imshow(img_rotate_unpadded)
        plt.subplot(1,3,3)
        plt.imshow(img_mask_rotate_unpadded)
        plt.show()
    
    return img_rotate_unpadded, img_mask_rotate_unpadded, angle, org_center, rot_center
 
    
 
"""
get_contour_features(img)
- Gets contour features from the image

Output:
features
"""

def get_contour_features(img):
    contours, _ = cv2.findContours(img, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
    for contour in contours:
        (x,y,w,h) = cv2.boundingRect(contour)
    ratio = round(w/h, 2)
    n_pixels = int(cv2.countNonZero(img))
    
    return ratio, n_pixels


    
def get_contours_and_blob(img):
    if len(img.shape)<3:
        img = cv2.merge([img,img,img])*255
        
    img_output = img.copy()
    gray = cv2.cvtColor(img_output, cv2.COLOR_BGR2GRAY)
    _, bw = cv2.threshold(gray, 50, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
    contours, _ = cv2.findContours(bw, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
    for i, pts in enumerate(contours):
        area = cv2.contourArea(pts)
        if area < 3700 or 100000 < area:
            continue
        cv2.drawContours(img_output, contours, i, (255, 255, 255), -1)
    return contours, img_output




'''



'''

def distance_function_skeleton(img):

    skel = skeletonize(img, method='lee')
    skel_cnt, _ = cv2.findContours(skel, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_NONE)
    
    return skel_cnt
  



"""
half_cut(img, cut=<string>, debug=<boolean>)
- Cuts image into half

Output:
img_cut = original image with cutting lines
half1 = half left/top
half2 = half right/bottom
half1_mask = half left/top mask
half2_mask = half right/bottom mask 
offset = pixel offset after cutting
"""

def half_cut(img, cut="vertical", apply_median=1, debug=0):
    img_cut = img.copy()
    half_x = int(img_cut.shape[1]/2)
    half_y = int(img_cut.shape[0]/2)  
    y = img_cut.shape[0]
    x = img_cut.shape[1]
    
    if cut == "horizontal":
        if debug == 1:
            cv2.line(img_cut, (int(0), int(half_y)), 
                     (img_cut.shape[1], int(half_y)), 
                     (255, 0, 0), 3, cv2.LINE_AA)
    
            
        half1 = img_cut[0:half_y, 0:x]
        tmp = cv2.cvtColor(half1, cv2.COLOR_BGR2GRAY)
        _, mask = cv2.threshold(tmp, 50, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
        if apply_median == 1:
            half1_mask = ndimage.median_filter(mask, size=5)
        else:
            half1_mask = mask
        
        half2 = img_cut[half_y:y, 0:x]
        tmp = cv2.cvtColor(half2, cv2.COLOR_BGR2GRAY)
        _, mask = cv2.threshold(tmp, 50, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
        if apply_median == 1:
            half2_mask = ndimage.median_filter(mask, size=5)
        else:
            half2_mask = mask
        
        
        offset = half_y
        
        
    
    if cut == "vertical":
        if debug == 1:
            cv2.line(img_cut, (half_x, 0), 
                 (half_x, img_cut.shape[1]), 
                 (255, 0, 0), 3, cv2.LINE_AA)
            
        half1 = img_cut[0:y, 0:half_x]
        tmp = cv2.cvtColor(half1, cv2.COLOR_BGR2GRAY)
        _, mask = cv2.threshold(tmp, 50, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
        if apply_median == 1:
            half1_mask = ndimage.median_filter(mask, size=5)
        else:
            half1_mask = mask
        
        
        half2 = img_cut[0:y, half_x:x]
        tmp = cv2.cvtColor(half2, cv2.COLOR_BGR2GRAY)
        _, mask = cv2.threshold(tmp, 50, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
        
        if apply_median == 1:
            half2_mask = ndimage.median_filter(mask, size=5)
        else:
            half2_mask = mask
            
        offset = half_x
    
    return img_cut, half1, half2, half1_mask, half2_mask, offset



def segment_by_quantization(img, n, threshold=1):    
    indices = np.arange(0,256)   # List of all colors 
    divider = np.linspace(0,255,n+1)[1] # we get a divider (255/n)
    quantiz = np.int0(np.linspace(0,255,n)) # we get quantization colors
    color_levels = np.clip(np.int0(indices/divider),0,n-1) # color levels 0,1,2..
    palette = quantiz[color_levels] # Creating the palette
    quantized = palette[img]  # Applying palette on image
    quantized = cv2.convertScaleAbs(quantized) # Converting image back to uint8
    
    if threshold == 1:
        if n == 2:
            th, quantized = cv2.threshold(quantized, 254, 255, cv2.THRESH_BINARY)
        if n == 3:
            th, quantized = cv2.threshold(quantized, 126, 255, cv2.THRESH_BINARY)
        if n == 4:
            th, quantized = cv2.threshold(quantized, int(255/3)-1, 255, cv2.THRESH_BINARY)
    return quantized


def get_child_contours(contours, hierarchy):
    
    child_contours = []
    child_hierarchy = []
    for cnt, h in zip(contours, hierarchy[0]):
        # print(h)
        if h[3] != - 1:
            child_contours.append(cnt)
            child_hierarchy.append(h)
    
    return child_contours, child_hierarchy