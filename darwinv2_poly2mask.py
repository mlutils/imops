import upolygon as up
import numpy as np
from darwin.utils import convert_polygons_to_sequences
from darwin.version import __version__




def gen_mask(polygon, height, width):
    """
    Takes a list of polygon points and returns a binary mask
    """
    mask = np.zeros((height, width))
    path = polygon
    sequences = convert_polygons_to_sequences(path)
    up.draw_polygon(mask, sequences, 1)
    # print(mask)
    return mask


def rle_encode(arr):
    """
    RLE encodes a numpy binary array into a mask
    """
    return up.rle_encode(arr)



def Array_AND(arr1,arr2):
    """
    A function that takes two multidimensional arrays (with the same dimensions) and returns an array where each component has undergone AND logic
    """
    arr1 = np.array(arr1)
    arr2 = np.array(arr2)

    flat_1 = Flatten(arr1)
    flat_2 = Flatten(arr2)

    and_array = np.logical_and(flat_1,flat_2)
    count_and_array = np.count_nonzero(and_array)

    return and_array, count_and_array


def Flatten(arr):
    """
    Returns a flattened array
    """
    return arr.flatten()


def Max_Array_Count(arr1,arr2):
    """
    Returns the max count for two binary arrays
    """
    narr1 = np.array(arr1)
    narr2 = np.array(arr2)

    counts1 = np.count_nonzero(narr1)
    counts2 = np.count_nonzero(narr2)
    
    return max(counts1,counts2)

def Array_Count(arr1):
    """
    Returns the non-zero count for an array
    """
    narr1 = np.array(arr1)
    counts1 = np.count_nonzero(narr1)
    return counts1

#Driver code to create masks from different files. Need to enter your own value instead of v2_paths in main
def main(poly_paths, height, width):

    #Initializing mask list and count
    mask_list = []
    masks_count = []


    for path in poly_paths:
        mask_list.append(gen_mask(path,height,width))
        masks_count.append(Array_Count(gen_mask(path,height,width)))

    #Finding index of largest mask (main mask from which others are subtracted)
    max_value = max(masks_count)
    max_val = [index for index, item in enumerate(masks_count) if item == max_value][0]


    #Defining main mask from which smaller masks (holes) are subtracted
    main_mask = mask_list.pop(max_val)

    #Subtract smaller masks from largest mask
    final_mask = main_mask

    for mask in mask_list:
        # final_mask = final_mask - mask

        # Check if the mask region is already part of final_mask
        # overlap = final_mask & mask  # Logical AND: non-zero where mask overlaps final_mask
        if np.any(np.logical_and(final_mask, mask)):  # If there's any overlap, subtract the mask
            final_mask = final_mask - mask  # Avoid negative values
        else:
            final_mask = final_mask + mask

    return final_mask
    #(Optional) RLE encode the final mask
    encoded_mask = rle_encode(final_mask)

    #(Optional) Checking final image
    from matplotlib import pyplot as plt
    plt.imshow(final_mask, interpolation='nearest')
    plt.show()


if __name__ == "__main__":
  #List of list of paths. Please enter your own here. It will be in 'paths'
    v2_paths = [
          [
            {
              "x": 67.83,
              "y": 83.22
            },
            {
              "x": 66.13,
              "y": 77.63
            },
            {
              "x": 62.85,
              "y": 74.76
            },
            {
              "x": 58.49,
              "y": 73.27
            },
            {
              "x": 55.99,
              "y": 73.94
            },
            {
              "x": 54.12,
              "y": 75.91
            },
            {
              "x": 51.45,
              "y": 76.62
            },
            {
              "x": 49.41,
              "y": 78.86
            },
            {
              "x": 48.54,
              "y": 82.6
            },
            {
              "x": 49.21,
              "y": 86.14
            },
            {
              "x": 52.69,
              "y": 90.24
            },
            {
              "x": 54.96,
              "y": 91.68
            },
            {
              "x": 58.29,
              "y": 92.35
            },
            {
              "x": 62.44,
              "y": 91.06
            },
            {
              "x": 66.13,
              "y": 87.99
            },
            {
              "x": 67.16,
              "y": 85.72
            }
          ],
          [
            {
              "x": 86.41,
              "y": 46.05
            },
            {
              "x": 86.21,
              "y": 42.33
            },
            {
              "x": 84.92,
              "y": 38.8
            },
            {
              "x": 82.26,
              "y": 36.56
            },
            {
              "x": 78.52,
              "y": 35.89
            },
            {
              "x": 73.34,
              "y": 36.97
            },
            {
              "x": 71.18,
              "y": 40.04
            },
            {
              "x": 65.73,
              "y": 44.17
            },
            {
              "x": 64.44,
              "y": 47.91
            },
            {
              "x": 65.11,
              "y": 51.44
            },
            {
              "x": 67.76,
              "y": 54.72
            },
            {
              "x": 71.69,
              "y": 56.78
            },
            {
              "x": 76.87,
              "y": 57.66
            },
            {
              "x": 80.82,
              "y": 56.37
            },
            {
              "x": 83.89,
              "y": 53.51
            },
            {
              "x": 86.21,
              "y": 47.08
            }
          ],
          [
            {
              "x": 89.51,
              "y": 127.0
            },
            {
              "x": 88.64,
              "y": 120.79
            },
            {
              "x": 85.77,
              "y": 117.72
            },
            {
              "x": 82.86,
              "y": 116.84
            },
            {
              "x": 78.52,
              "y": 117.25
            },
            {
              "x": 73.96,
              "y": 118.96
            },
            {
              "x": 69.86,
              "y": 122.85
            },
            {
              "x": 68.57,
              "y": 126.38
            },
            {
              "x": 69.44,
              "y": 131.16
            },
            {
              "x": 72.1,
              "y": 134.02
            },
            {
              "x": 74.58,
              "y": 135.67
            },
            {
              "x": 80.38,
              "y": 137.37
            },
            {
              "x": 83.91,
              "y": 136.5
            },
            {
              "x": 85.74,
              "y": 134.67
            },
            {
              "x": 88.22,
              "y": 131.77
            },
            {
              "x": 89.31,
              "y": 128.24
            }
          ],
          [
            {
              "x": 122.97,
              "y": 86.32
            },
            {
              "x": 122.76,
              "y": 82.81
            },
            {
              "x": 120.64,
              "y": 76.18
            },
            {
              "x": 113.03,
              "y": 68.36
            },
            {
              "x": 107.43,
              "y": 66.45
            },
            {
              "x": 99.79,
              "y": 66.87
            },
            {
              "x": 92.96,
              "y": 69.19
            },
            {
              "x": 90.71,
              "y": 71.43
            },
            {
              "x": 89.89,
              "y": 73.5
            },
            {
              "x": 89.22,
              "y": 76.62
            },
            {
              "x": 90.0,
              "y": 79.34
            },
            {
              "x": 86.79,
              "y": 82.58
            },
            {
              "x": 85.14,
              "y": 86.3
            },
            {
              "x": 84.47,
              "y": 92.72
            },
            {
              "x": 86.48,
              "y": 98.74
            },
            {
              "x": 87.0,
              "y": 103.07
            },
            {
              "x": 90.89,
              "y": 107.38
            },
            {
              "x": 96.08,
              "y": 109.49
            },
            {
              "x": 103.33,
              "y": 108.62
            },
            {
              "x": 110.97,
              "y": 105.11
            },
            {
              "x": 115.89,
              "y": 99.56
            },
            {
              "x": 116.77,
              "y": 93.26
            },
            {
              "x": 119.64,
              "y": 92.3
            },
            {
              "x": 121.88,
              "y": 90.06
            },
            {
              "x": 122.76,
              "y": 87.09
            }
          ],
          [
            {
              "x": 143.82,
              "y": 43.99
            },
            {
              "x": 143.15,
              "y": 41.49
            },
            {
              "x": 141.32,
              "y": 39.66
            },
            {
              "x": 138.82,
              "y": 38.99
            },
            {
              "x": 124.57,
              "y": 40.43
            },
            {
              "x": 119.39,
              "y": 41.93
            },
            {
              "x": 117.15,
              "y": 43.96
            },
            {
              "x": 116.27,
              "y": 47.29
            },
            {
              "x": 116.68,
              "y": 49.36
            },
            {
              "x": 119.42,
              "y": 54.95
            },
            {
              "x": 123.93,
              "y": 58.64
            },
            {
              "x": 131.39,
              "y": 60.34
            },
            {
              "x": 137.61,
              "y": 59.67
            },
            {
              "x": 140.88,
              "y": 57.02
            },
            {
              "x": 141.76,
              "y": 52.25
            },
            {
              "x": 140.69,
              "y": 48.49
            },
            {
              "x": 143.15,
              "y": 46.49
            }
          ],
          [
            {
              "x": 168.26,
              "y": 89.0
            },
            {
              "x": 168.26,
              "y": 79.0
            },
            {
              "x": 167.26,
              "y": 78.0
            },
            {
              "x": 167.26,
              "y": 72.0
            },
            {
              "x": 166.26,
              "y": 71.0
            },
            {
              "x": 166.26,
              "y": 68.0
            },
            {
              "x": 165.26,
              "y": 67.0
            },
            {
              "x": 164.26,
              "y": 62.0
            },
            {
              "x": 156.26,
              "y": 48.0
            },
            {
              "x": 142.26,
              "y": 35.0
            },
            {
              "x": 122.26,
              "y": 25.0
            },
            {
              "x": 116.26,
              "y": 24.0
            },
            {
              "x": 115.26,
              "y": 23.0
            },
            {
              "x": 109.26,
              "y": 23.0
            },
            {
              "x": 108.26,
              "y": 22.0
            },
            {
              "x": 90.26,
              "y": 23.0
            },
            {
              "x": 86.26,
              "y": 25.0
            },
            {
              "x": 83.26,
              "y": 25.0
            },
            {
              "x": 82.26,
              "y": 26.0
            },
            {
              "x": 77.26,
              "y": 27.0
            },
            {
              "x": 69.26,
              "y": 31.0
            },
            {
              "x": 62.26,
              "y": 37.0
            },
            {
              "x": 61.26,
              "y": 37.0
            },
            {
              "x": 51.26,
              "y": 48.0
            },
            {
              "x": 51.26,
              "y": 49.0
            },
            {
              "x": 45.26,
              "y": 56.0
            },
            {
              "x": 39.26,
              "y": 68.0
            },
            {
              "x": 39.26,
              "y": 70.0
            },
            {
              "x": 37.26,
              "y": 74.0
            },
            {
              "x": 35.26,
              "y": 86.0
            },
            {
              "x": 34.26,
              "y": 87.0
            },
            {
              "x": 34.26,
              "y": 96.0
            },
            {
              "x": 35.26,
              "y": 97.0
            },
            {
              "x": 35.26,
              "y": 103.0
            },
            {
              "x": 36.26,
              "y": 104.0
            },
            {
              "x": 36.26,
              "y": 108.0
            },
            {
              "x": 37.26,
              "y": 109.0
            },
            {
              "x": 37.26,
              "y": 111.0
            },
            {
              "x": 39.26,
              "y": 114.0
            },
            {
              "x": 39.26,
              "y": 116.0
            },
            {
              "x": 41.26,
              "y": 119.0
            },
            {
              "x": 41.26,
              "y": 121.0
            },
            {
              "x": 43.26,
              "y": 125.0
            },
            {
              "x": 45.26,
              "y": 127.0
            },
            {
              "x": 49.26,
              "y": 134.0
            },
            {
              "x": 60.26,
              "y": 144.0
            },
            {
              "x": 61.26,
              "y": 144.0
            },
            {
              "x": 66.26,
              "y": 148.0
            },
            {
              "x": 68.26,
              "y": 148.0
            },
            {
              "x": 75.26,
              "y": 152.0
            },
            {
              "x": 78.26,
              "y": 152.0
            },
            {
              "x": 84.26,
              "y": 155.0
            },
            {
              "x": 94.26,
              "y": 156.0
            },
            {
              "x": 95.26,
              "y": 157.0
            },
            {
              "x": 102.26,
              "y": 157.0
            },
            {
              "x": 105.26,
              "y": 155.0
            },
            {
              "x": 112.26,
              "y": 155.0
            },
            {
              "x": 113.26,
              "y": 154.0
            },
            {
              "x": 121.26,
              "y": 153.0
            },
            {
              "x": 125.26,
              "y": 150.0
            },
            {
              "x": 127.26,
              "y": 150.0
            },
            {
              "x": 133.26,
              "y": 147.0
            },
            {
              "x": 136.26,
              "y": 144.0
            },
            {
              "x": 137.26,
              "y": 144.0
            },
            {
              "x": 147.26,
              "y": 133.0
            },
            {
              "x": 147.26,
              "y": 132.0
            },
            {
              "x": 151.26,
              "y": 127.0
            },
            {
              "x": 152.26,
              "y": 124.0
            },
            {
              "x": 155.26,
              "y": 121.0
            },
            {
              "x": 156.26,
              "y": 118.0
            },
            {
              "x": 160.26,
              "y": 113.0
            },
            {
              "x": 160.26,
              "y": 111.0
            },
            {
              "x": 162.26,
              "y": 109.0
            },
            {
              "x": 162.26,
              "y": 107.0
            },
            {
              "x": 163.26,
              "y": 106.0
            },
            {
              "x": 163.26,
              "y": 104.0
            },
            {
              "x": 164.26,
              "y": 103.0
            },
            {
              "x": 164.26,
              "y": 101.0
            },
            {
              "x": 165.26,
              "y": 100.0
            },
            {
              "x": 165.26,
              "y": 98.0
            },
            {
              "x": 167.26,
              "y": 94.0
            },
            {
              "x": 167.26,
              "y": 90.0
            }
          ]
        ]




    #Example heights and widths. Should enter values extracted from the export file
    height = 277
    width = 640


    main(v2_paths, height, width)


